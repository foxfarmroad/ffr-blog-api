var AWS = require('aws-sdk');

function putSsmParameter (ssm, params) {
  params.forEach(param => {
    ssm.putParameter(param, function(err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      } else {
        console.log(data);           // successful response
      }
    });
  });
}

// TODO getParameter and check version to only update when the value updated?

function handler (data, serverless, options) {
  AWS.config.update({region: serverless.service.provider.region});
  var ssm = new AWS.SSM();

  var params = [];

  params.push(
    {
      Name: `/${serverless.service.provider.stage}/ServiceEndpoint`, /* required */
      Type: 'String', /* required */
      Value: data['ServiceEndpoint'], /* required */
      Overwrite: true
    }
  )

  params.push(
    {
      Name: `/${serverless.service.provider.stage}/MaxAttachmentSize`, /* required */
      Type: 'String', /* required */
      Value: serverless.service.custom.maxAttachmentSize, /* required */
      Overwrite: true
    }
  )

  putSsmParameter(ssm, params);
}
 
module.exports = { handler }