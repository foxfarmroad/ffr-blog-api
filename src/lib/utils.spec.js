const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

const sinonChai = require('sinon-chai');
chai.use(sinonChai);
// ** Important **
// chai-as-promised must be added to chai last for these to work together
const chaiArrays = require('chai-arrays');
chai.use(chaiArrays);
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

import {ResponseUtils, ValidationUtils} from "./utils";

import {SlugUtils} from "./utils";
import * as stopwords from '../config/stopwords';

import {ContentUtils} from './utils';

import {DatabaseUtils} from "./utils";
import AWS from 'aws-sdk';

import {UUIDUtils} from "./utils";

const guidPattern = /[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/;

describe('The utils', function () {
  before(function () {
    chai.use(sinonChai);
  });

  beforeEach(function () {
    this.sandbox = sinon.sandbox.create();
  });

  afterEach(function () {
    this.sandbox.restore();
  });

  describe('ValidationUtils module', function () {
    before(function () {
      this.valUtils = new ValidationUtils();
    });

    beforeEach(function () {
      this.sandbox.spy(this.valUtils.v, 'validate');
    });

    afterEach(function () {
      this.valUtils.v.validate.restore();
    });

    describe('interacts with jsonschema', function () {
      it('once', function () {
        this.valUtils.validate({body:'{}'}, require("../../schema/collection/post.json")).catch(() => {
        });
        return expect(this.valUtils.v.validate).to.be.calledOnce;
      });
    });

    it('rejects an invalid schema file', function () {
      return expect(this.valUtils.validate({}, "test.json")).to.be.rejected
        .and.eventually.have.property('statusCode')
        .and.eventually.equal(500);
    });

    it('rejects object missing required properties', function () {
      return expect(this.valUtils.validate({body:'{}'}, require("../../schema/collection/post.json"))).to.be.rejected
        .and.eventually.have.property('statusCode')
        .and.eventually.equal(400);
    });

    it('fulfills object that matches schema', function () {
      return expect(this.valUtils.validate({body:'{"title": "test", "content": "test", "tags": ["test"]}'},
        require("../../schema/collection/post.json"))).to.eventually.be.fulfilled;
    });
  });

  describe('SlugUtils module', function () {
    let stopwordsMock;

    before(function () {
      this.slugUtils = new SlugUtils();
    });

    beforeEach(function () {
      stopwordsMock = this.sandbox.mock(stopwords);
    });

    afterEach(function () {
      stopwordsMock.restore();
    });

    describe('interacts with stopwords', function () {
      it('never when input is not an array', function () {
        this.slugUtils.removeStopWords({}).catch(() => {
        });
        return stopwordsMock.expects('words').never();
      });

      it('once when input is an array', function () {
        this.slugUtils.removeStopWords([]).catch(() => {
        });
        return stopwordsMock.expects('words').never();
      });
    });

    it('rejects non-array values', function () {
      return expect(this.slugUtils.removeStopWords({})).to.be.rejectedWith(Error,
        `Not an array!`);
    });

    it('transforms an empty array into an empty array', function () {
      return expect(this.slugUtils.removeStopWords([])).to.eventually.deep.equal([]);
    });

    it('removes all stop words when they are all stop words', function () {
      return expect(this.slugUtils.removeStopWords(['a', 'about', 'they\'re', 'your'])).to.eventually.deep.equal([]);
    });

    it('removes all stop words when some are stop words', function () {
      return expect(this.slugUtils.removeStopWords(['i\'m', 'about', 'done', 'here'])).to.eventually.deep.equal(['done']);
    });

    it('rejects generating a slug when not a string', function () {
      return expect(this.slugUtils.generate({})).to.be.rejectedWith(Error, 'Not a string!');
    });

    it('rejects generating a slug given a string that is too low entropy', function () {
      return expect(this.slugUtils.generate('test')).to.be.rejectedWith(Error, 'Not enough entropy to create a slug!');
    });

    it('fulfills generating a slug given a string with no stop words', function () {
      return expect(this.slugUtils.generate('test test')).to.eventually.equal('test-test');
    });

    it('fulfills generating a slug given a string with stop words', function () {
      return expect(this.slugUtils.generate('test a test it loves')).to.eventually.equal('test-test-loves');
    });
  });

  describe('ContentUtils module', function () {
    const now = new Date().getTime();
    const slugUtils = new SlugUtils();
    let generateStub;

    beforeEach(function () {
      generateStub = this.sandbox.stub(slugUtils, 'generate');
    });

    afterEach(function () {
      generateStub.restore();
    });

    describe('generateJournalParams', function () {
      const event = {
        requestContext: {
          identity: {
            cognitoIdentityId: 'userId'
          }
        },
        body: JSON.stringify({
          title: 'title',
          content: 'content',
          tags: ['tag1', 'tag2']
        })
      };

      it('rejects empty input', function () {
        return expect(ContentUtils.generateJournalParams(slugUtils, now, "Table", {}, "NO-OP")).to.be.rejected
          .and.eventually.have.property('statusCode')
          .and.eventually.equal(500);
      });

      it('rejects malformed input', function () {
        return expect(ContentUtils.generateJournalParams(slugUtils, now, "Table", {body:{}}, "NO-OP")).to.be.rejected
          .and.eventually.have.property('statusCode')
          .and.eventually.equal(500);
      });

      it('fulfills expected input', function () {
        generateStub.resolves('test-slug');
        this.sandbox.stub(UUIDUtils, 'guid').returns("abcd1234-ab12-ab12-ab12-abcdef123456");
        return expect(ContentUtils.generateJournalParams(slugUtils, now, "Table", event, "NO-OP")).to.eventually.deep.equal(
          {
            TableName: 'Table',
            Item: {
              userId: 'userId',
              uuId: UUIDUtils.guid(),
              slug: 'test-slug',
              title: 'title',
              content: 'content',
              tags: ['tag1', 'tag2'],
              created: now,
              operation: 'NO-OP'
            }
          }
        );
      });
    });

    describe('generateBlogSnapshotParams', function () {
      const object = {
        slug: 'test-slug'
      };

      it('fulfills expected input', function () {
        return expect(ContentUtils.generateBlogSnapshotParams("Table", object)).to.eventually.deep.equal(
          {
            TableName: "Table",
            Key: {
              'slug': {
                S: 'test-slug'
              }
            }
          }
        );
      });
    });

    describe('generateBlogSnapshotScanParams', function () {
      const object = {
        lastEvaluatedKey: {'slug': 'test-slug', 'postDate': 'test-date'}
      };

      it('fulfills without paging', function () {
        return expect(ContentUtils.generateBlogSnapshotScanParams("Table", {})).to.eventually.deep.equal(
          {
            TableName: "Table",
            Limit: 10
          }
        )
      });

      it('fulfills with paging', function () {
        return expect(ContentUtils.generateBlogSnapshotScanParams("Table", object)).to.eventually.deep.equal(
          {
            TableName: "Table",
            ExclusiveStartKey: {'slug': 'test-slug', 'postDate': 'test-date'}
          }
        );
      });
    });
  });

  describe('DatabaseUtils module', function () {
    const database = new AWS.DynamoDB.DocumentClient();
    let putStub;
    let getStub;
    let scanStub;

    const headers = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    };

    beforeEach(function () {
      putStub = this.sandbox.stub(database, 'put');
      getStub = this.sandbox.stub(database, 'get');
      scanStub = this.sandbox.stub(database, 'scan');
    });

    describe('saveItem', function () {
      it('rejects when error', function () {
        putStub.withArgs(sinon.match.any).yields(new Error(), null);
        return expect(DatabaseUtils.saveItem(database, {})).to.be.rejected.and.eventually.deep.equal(
          {
            statusCode: 500,
            headers: headers,
            body: '{"status":false,"error":"Failed database put!"}'
          }
        );
      });

      it('fulfills when no error', function () {
        putStub.withArgs(sinon.match.any).yields(null, null);
        return expect(DatabaseUtils.saveItem(database, {})).to.eventually.deep.equal(
          {
            statusCode: 201,
            headers: headers,
            body: '{"status":true}'
          }
        );
      });
    });

    describe('getItem', function () {
      const object = {
        Item: {
          slug: 'test-slug',
          title: 'Test Title',
          content: 'Test Content',
          tags: [
            'testTag1', 'testTag2'
          ],
          updated: 'testdate'
        }
      };

      it('rejects when error', function () {
        getStub.withArgs(sinon.match.any).yields(new Error(), null);
        return expect(DatabaseUtils.getItem(database, {})).to.be.rejected.and.eventually.deep.equal(
          {
            statusCode: 500,
            headers: headers,
            body: '{"status":false,"error":"Failed database get!"}'
          }
        );
      });

      it('fulfills when item found', function () {
        getStub.withArgs(sinon.match.any).yields(null, object);
        return expect(DatabaseUtils.getItem(database, {})).to.eventually.deep.equal(
          {
            statusCode: 200,
            headers: headers,
            body: {
              slug: 'test-slug',
              title: 'Test Title',
              content: 'Test Content',
              tags: [
                'testTag1', 'testTag2'
              ],
              updated: 'testdate'
            }
          }
        );
      });

      it('rejects when item NOT found', function () {
        getStub.withArgs(sinon.match.any).yields(null, {});
        return expect(DatabaseUtils.getItem(database, {})).to.be.rejected.and.eventually.deep.equal(
          {
            statusCode: 404,
            headers: headers
          }
        );
      });
    });

    describe('scan', function () {
      const object = {
        Items: [
          {
            slug: 'test-slug',
            title: 'Test Title',
            content: 'Test Content',
            tags: [
              'testTag1', 'testTag2'
            ],
            updated: 'testdate'
          },
          {
            slug: 'test-slug',
            title: 'Test Title',
            content: 'Test Content',
            tags: [
              'testTag1', 'testTag2'
            ],
            updated: 'testdate'
          }
        ]
      };

      it('rejects when error', function () {
        scanStub.withArgs(sinon.match.any).yields(new Error(), null);
        return expect(DatabaseUtils.scan(database, {})).to.be.rejected.and.eventually.deep.equal(
          {
            statusCode: 500,
            headers: headers,
            body: '{"status":false,"error":"Failed database scan!"}'
          }
        );
      });

      it('fulfills when items found', function () {
        scanStub.withArgs(sinon.match.any).yields(null, object);
        return expect(DatabaseUtils.scan(database, {})).to.eventually.deep.equal(
          {
            statusCode: 200,
            headers: headers,
            body: [
              {
                slug: 'test-slug',
                title: 'Test Title',
                content: 'Test Content',
                tags: [
                  'testTag1', 'testTag2'
                ],
                updated: 'testdate'
              },
              {
                slug: 'test-slug',
                title: 'Test Title',
                content: 'Test Content',
                tags: [
                  'testTag1', 'testTag2'
                ],
                updated: 'testdate'
              }
            ]
          }
        );
      });

      it('rejects when items NOT found', function () {
        scanStub.withArgs(sinon.match.any).yields(null, {});
        return expect(DatabaseUtils.scan(database, {})).to.be.rejected.and.eventually.deep.equal(
          {
            statusCode: 404,
            headers: headers
          }
        );
      });
    });
  });

  describe('UUIDUtils module', function () {
    describe('s4', function () {
      it('generates random 4 hex characters', function () {
        return expect(UUIDUtils.s4()).to.match(/[a-f0-9]{4}/);
      });
    });

    describe('guid', function () {
      it('generates a correctly formatted id', function () {
        return expect(UUIDUtils.guid()).to.match(guidPattern);
      });
    })
  });

  describe('ResponseUtils module', function () {
    const headers = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    };

    describe('internalServerError', function () {
      it('generates a 500 response', function () {
        return expect(ResponseUtils.internalServerError({})).to.deep.equal(
          {
            statusCode: 500,
            headers: headers,
            body: '{\"status\":false,\"error\":{}}'
          }
        );
      });
    });
  });
});