import {Validator} from 'jsonschema';
import * as stopwords from '../config/stopwords';

class ValidationUtils {
  constructor() {
    this.v = new Validator();
  }
  validate(event, schema) {
    return new Promise((resolve, reject) => {
      try {
        let object;
        if (event.body) {
          object = JSON.parse(event.body);
        } else {
          object = {};
        }
        const validation = this.v.validate(object, schema);
        if (validation.errors.length > 0) {
          reject(ResponseUtils.badRequestError(validation));
        } else {
          resolve(object);
        }
      } catch (err) {
        if (!err.message) {
          reject(ResponseUtils.internalServerError("Failed validating body!"));
        } else {
          reject(ResponseUtils.internalServerError(err.message));
        }
      }
    });
  }
}

class SlugUtils {
  constructor() {
  }

  generate(input) {
    return new Promise((resolve, reject) => {
      try {
        if (typeof input !== 'string') {
          reject(new Error('Not a string!'));
        }
        const wordList = input.toLowerCase().split(' ');
        if (wordList.length < 2) {
          reject(new Error('Not enough entropy to create a slug!'));
        }
        this.removeStopWords(wordList).then(newList => {
          resolve(newList.join('-'));
        }, err => {
          reject(err);
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  removeStopWords(array) {
    return new Promise((resolve, reject) => {
      if (!Array.isArray(array)) {
        reject(new Error('Not an array!'));
      }

      let newState = [];

      array.forEach(element => {
        if (stopwords.words().indexOf(element) < 0) {
          newState.push(element);
        }
      });

      resolve(newState);
    });
  }
}

class ContentUtils {
  static generateJournalParams(slugUtils, now, tableName, event, operation) {
    return new Promise((resolve, reject) => {
      let params = {
        TableName: tableName,
        Item: {
          uuId: UUIDUtils.guid(),
          created: now,
          operation: operation
        }
      };

      try {
        if (event.requestContext.identity.cognitoIdentityId) {
          params.Item.userId = event.requestContext.identity.cognitoIdentityId;
        }
        if (event.body) {
          let body = JSON.parse(event.body);
          if (!body.slug) {
            slugUtils.generate(body.title)
              .then(slug => params.Item.slug = slug)
              .catch(err => {
                if (!err.message) {
                  reject(ResponseUtils.internalServerError("Failed generating slug!"));
                } else {
                  reject(ResponseUtils.internalServerError(err.message));
                }
              });
          } else {
            params.Item.slug = body.slug;
          }
          if (body.title) {
            params.Item.title = body.title;
          }
          if (body.content) {
            params.Item.content = body.content;
          }
          if (body.tags) {
            params.Item.tags = body.tags;
          }
        }
        resolve(params);
      } catch (err) {
        if (!err.message) {
          reject(ResponseUtils.internalServerError("Failed generating journal params!"));
        } else {
          reject(ResponseUtils.internalServerError(err.message));
        }
      }
    });
  }

  static generateBlogSnapshotParams(tableName, object) {
    return new Promise((resolve, reject) => {
      try {
        const params = {
          TableName: tableName,
          Key: {
            'slug': {
              S: object.slug
            }
          }
        };
        resolve(params);
      } catch (err) {
        if (!err.message) {
          reject(ResponseUtils.internalServerError("Failed generating blog params!"));
        } else {
          reject(ResponseUtils.internalServerError(err.message));
        }
      }
    });
  }

  static generateBlogSnapshotScanParams(tableName, object) {
    return new Promise((resolve, reject) => {
      try {
        const params = {
          TableName: tableName,
        };
        if (object.lastEvaluatedKey) {
          params.ExclusiveStartKey = object.lastEvaluatedKey
        } else {
          params.Limit = 10
        }
        resolve(params);
      } catch (err) {
        if (!err.message) {
          reject(ResponseUtils.internalServerError("Failed generating blog scan params!"));
        } else {
          reject(ResponseUtils.internalServerError(err.message));
        }
      }
    });
  }
}

class DatabaseUtils {
  static saveItem(database, params) {
    return new Promise((resolve, reject) => {
      database.put(params, (err) => {
        if (err) {
          if (!err.message) {
            reject(ResponseUtils.internalServerError("Failed database put!"));
          } else {
            reject(ResponseUtils.internalServerError(err.message));
          }
        } else {
          resolve(ResponseUtils.createdSuccess());
        }
      });
    });
  }

  static getItem(database, params) {
    return new Promise((resolve, reject) => {
      database.get(params, (err, data) => {
        if (err) {
          if (!err.message) {
            reject(ResponseUtils.internalServerError("Failed database get!"));
          } else {
            reject(ResponseUtils.internalServerError(err.message));
          }
        } else if (data) {
          if (data.Item) {
            const response = {
              statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin" : "*",
                "Access-Control-Allow-Credentials" : true
              },
              body: {
                slug: data.Item.slug,
                title: data.Item.title,
                content: data.Item.content,
                tags: data.Item.tags,
                updated: data.Item.updated
              }
            };
            resolve(response);
          } else {
            reject(ResponseUtils.notFound());
          }
        }
      });
    });
  }

  static scan(database, params) {
    return new Promise((resolve, reject) => {
      database.scan(params, (err, data) => {
        if (err) {
          if (!err.message) {
            reject(ResponseUtils.internalServerError("Failed database scan!"));
          } else {
            reject(ResponseUtils.internalServerError(err.message));
          }
        } else if (data) {
          if (data.Items && Array.isArray(data.Items)) {
            const response = {
              statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin" : "*",
                "Access-Control-Allow-Credentials" : true
              },
              body: data.Items
            };
            resolve(response);
          } else {
            reject(ResponseUtils.notFound());
          }
        }
      });
    });
  }
}

class UUIDUtils {
  static s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  static guid() {
    return UUIDUtils.s4() + UUIDUtils.s4() + '-' + UUIDUtils.s4() + '-' + UUIDUtils.s4() + '-' + UUIDUtils.s4() + '-' +
      UUIDUtils.s4() + UUIDUtils.s4() + this.s4();
  }
}

class ResponseUtils {
  static internalServerError(message) {
    return {
      statusCode: 500,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: JSON.stringify(
        {
          status: false,
          error: message
        }
      )
    };
  }

  static badRequestError(validation) {
    return {
      statusCode: 400,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: JSON.stringify(
        {
          status: false,
          errors: validation.errors
        }
      )
    };
  }

  static createdSuccess() {
    return {
      statusCode: 201,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: JSON.stringify({ status: true })
    };
  }

  static notFound() {
    return {
      statusCode: 404,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      }
    };
  }
}

class EventUtils {
  static chooseConfig(event, configs) {
    const id = (event["pathParameters"] !== null && "slug" in event["pathParameters"]) ?
        event["pathParameters"]["slug"] :
        undefined;
    return (id === undefined) ? configs.collection : configs.item;
  }
}

class HandlerUtils {
  static touchJournal(validationUtils, event, config, database, callback, slugUtils, now) {
    return validationUtils.validate(event, config.schema)
      .then(object => config.paramsFunc(slugUtils, now, config.tableName, event, config.operation))
      .then(params => config.databaseFunc(database, params))
      .then(success => callback(null, success))
      .catch(failure => callback(null, failure));
    // TODO Trigger write view to blog snapshot table using lambda function
  }

  static touchSnapshot(validationUtils, event, config, database, callback) {
    return validationUtils.validate(event, config.schema)
      .then(object => config.paramsFunc(config.tableName, object))
      .then(params => config.databaseFunc(database, params))
      .then(success => callback(null, success))
      .catch(failure => callback(null, failure));
  }
}

export {ValidationUtils, SlugUtils, ContentUtils, DatabaseUtils, UUIDUtils, ResponseUtils, EventUtils, HandlerUtils};