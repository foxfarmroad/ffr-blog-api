const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

const sinonChai = require('sinon-chai');
chai.use(sinonChai);
// ** Important **
// chai-as-promised must be added to chai last for these to work together
const chaiArrays = require('chai-arrays');
chai.use(chaiArrays);
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

import {ContentUtils, DatabaseUtils, SlugUtils, ValidationUtils} from "../lib/utils";

import { operationTypes, router } from "./content";

import * as awsMock from 'aws-sdk-mock';
import AWS from 'aws-sdk';

describe('Content handler', function () {
  before(function () {
    chai.use(sinonChai);
    awsMock.mock('DynamoDB.DocumentClient', 'get', function (params, callback) {
      callback(null, {Item:{slug:"test",title:"test",content:"test",tags:[],updated:"test"}});
    });
    awsMock.mock('DynamoDB.DocumentClient', 'put', function (params, callback) {
      callback(null, "successfully put item in database");
    });
    awsMock.mock('DynamoDB.DocumentClient', 'scan', function (params, callback) {
      callback(null, {Items: []});
    });
  });

  beforeEach(function () {
    this.sandbox = sinon.sandbox.create();
  });

  afterEach(function () {
    this.sandbox.restore();
  });

  describe('router', function () {

    describe('calls get collection', function () {

      it('when valid schema file', function () {
        router({pathParameters:null,httpMethod:"GET",body:null}, {}, function (error, message) {
          console.log(message);
        });
      });

    });

    describe('calls get item', function () {

      it('when valid schema file', function () {
        router({pathParameters:{slug:"test"},httpMethod:"GET",body:null}, {}, function (error, message) {
          console.log(message);
        });
      });

    });

    describe('calls post item', function () {

      before(function () {
        process.env.CONTENT_JOURNAL_TABLE = "Table";
      });

      it('when valid schema file', function () {
        router({requestContext:{identity:{}},pathParameters:null,httpMethod:"POST",
          body:"{\"title\":\"Title\",\"content\":\"Content\",\"tags\":[\"tag1\"]}"}, {}, function (error, message) {
          console.log(message);
        });
      });

    });

  });

});