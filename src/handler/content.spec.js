const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

const sinonChai = require('sinon-chai');
chai.use(sinonChai);
// ** Important **
// chai-as-promised must be added to chai last for these to work together
const chaiArrays = require('chai-arrays');
chai.use(chaiArrays);
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

import {DatabaseUtils, SlugUtils, ValidationUtils, EventUtils, HandlerUtils} from "../lib/utils";

import { operationTypes } from "./content";

import * as awsMock from 'aws-sdk-mock';
import AWS from 'aws-sdk';

describe('Content handler', function () {
  before(function () {
    chai.use(sinonChai);

    awsMock.mock('DynamoDB.DocumentClient', 'put', function (params, callback) {
      callback(null, "successfully put item in database");
    });
    this.database = new AWS.DynamoDB.DocumentClient();
    this.databaseUtils = new DatabaseUtils();
  });

  beforeEach(function () {
    this.sandbox = sinon.sandbox.create();
  });

  afterEach(function () {
    this.sandbox.restore();
  });

  describe('touchJournal', function () {

    before(function () {
      this.valUtils = new ValidationUtils();
    });

    let validateStub;
    let generateParamsFuncStub;
    let databaseFuncStub;

    beforeEach(function () {
      validateStub = this.sandbox.stub(ValidationUtils.prototype, 'validate');
      generateParamsFuncStub = this.sandbox.stub();
      databaseFuncStub = this.sandbox.stub();
    });

    afterEach(function () {
      validateStub.restore();
    });

    describe('interacts with generateParamsFunc', function () {

      it('never when invalid schema file', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(generateParamsFuncStub).callCount(0);
        });
      });

      it('once when valid schema file', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects('rejected!');
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(generateParamsFuncStub).to.be.calledOnce;
        });
      });
    });

    describe('interacts with databaseFunc', function () {

      it('never when validate rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).callCount(0);
        });
      });

      it('never when generateJournalParams rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).callCount(0);
          });
      });

      it('once when validate and generateJournalParams resolve', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).to.be.calledOnce;
        });
      });
    });

    describe('interacts with callback', function () {

      it('once when validate rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });

      it('once when generateParamsFuncStub rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });

      it('once when databaseFunc rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback).to.be.calledOnce;
        });
      });

      it('once when all resolve', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });
    });

    describe('is successful', function () {

      it('with correct response', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[1]).deep.equal({});
        });
      });

      it('with no error', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[0]).deep.equal(null);
        });
      });
    });

    describe('fails', function () {

      it('with correct response', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[1]).deep.equal({});
        });
      });

      it('with no error', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchJournal(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, operation:operationTypes["CREATE"], databaseFunc:databaseFuncStub},
          this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[0]).deep.equal(null);
        });
      });
    });
  });

  describe('touchSnapshot', function () {

    before(function () {
      this.valUtils = new ValidationUtils();
    });

    let validateStub;
    let generateParamsFuncStub;
    let databaseFuncStub;

    beforeEach(function () {
      validateStub = this.sandbox.stub(ValidationUtils.prototype, 'validate');
      generateParamsFuncStub = this.sandbox.stub();
      databaseFuncStub = this.sandbox.stub();
    });

    afterEach(function () {
      validateStub.restore();
    });

    describe('interacts with generateParamsFunc', function () {

      it('never when invalid schema file', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(generateParamsFuncStub).callCount(0);
        });
      });

      it('once when valid schema file', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects('rejected!');
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(generateParamsFuncStub).to.be.calledOnce;
        });
      });
    });

    describe('interacts with databaseFunc', function () {

      it('never when validate rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).callCount(0);
        });
      });

      it('never when generateJournalParams rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).callCount(0);
        });
      });

      it('once when validate and generateJournalParams resolve', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(databaseFuncStub).to.be.calledOnce;
        });
      });
    });

    describe('interacts with callback', function () {

      it('once when validate rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });

      it('once when generateParamsFuncStub rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });

      it('once when databaseFunc rejects', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback).to.be.calledOnce;
        });
      });

      it('once when all resolve', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback).calledOnce;
        });
      });
    });

    describe('is successful', function () {

      it('with correct response', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[1]).deep.equal({});
        });
      });

      it('with no error', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.resolves({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[0]).deep.equal(null);
        });
      });
    });

    describe('fails', function () {

      it('with correct response', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[1]).deep.equal({});
        });
      });

      it('with no error', function () {
        const callback = this.sandbox.spy();
        validateStub.resolves({});
        generateParamsFuncStub.resolves({});
        databaseFuncStub.rejects({});
        return HandlerUtils.touchSnapshot(this.valUtils, {body:"{}"}, {schema:"test.json",
          paramsFunc:generateParamsFuncStub, databaseFunc:databaseFuncStub}, this.database, callback)
        .then(() => {
          return expect(callback.getCall(0).args[0]).deep.equal(null);
        });
      });
    });
  });
});