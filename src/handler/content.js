import AWS from "aws-sdk";
import {ValidationUtils, ContentUtils, DatabaseUtils, SlugUtils, EventUtils, HandlerUtils} from '../lib/utils';

import getItem from "../../schema/item/get.json";
import putItem from "../../schema/item/put.json";
import deleteItem from "../../schema/item/delete.json";
import postCollection from "../../schema/collection/post.json";
import getCollection from "../../schema/collection/get.json";

const validationUtils = new ValidationUtils();
const slugUtils = new SlugUtils();
AWS.config.update({ region: process.env.REGION });

export const operationTypes = {
  "CREATE": "7588E341-63AF-4785-9CC4-FDF6B780F2DC",
  "UPDATE": "EF0947E2-C6E8-4B8F-AD78-EC93332A247F",
  "DELETE": "54BF54CA-1F8D-44DE-A813-82D693CBE50D"
};

export function router(event, context, callback) {
  const itemConfigs = {
    "GET": {
      schema: getItem,
      paramsFunc: ContentUtils.generateBlogSnapshotParams,
      databaseFunc: DatabaseUtils.getItem,
      tableName: process.env.BLOG_SNAPSHOT_TABLE,
      util: HandlerUtils.touchSnapshot
    },
    "PUT": {
      schema: putItem,
      paramsFunc: ContentUtils.generateJournalParams,
      databaseFunc: DatabaseUtils.saveItem,
      operation: operationTypes["UPDATE"],
      tableName: process.env.CONTENT_JOURNAL_TABLE,
      util: HandlerUtils.touchJournal
    },
    "DELETE": {
      schema: deleteItem,
      paramsFunc: ContentUtils.generateJournalParams,
      databaseFunc: DatabaseUtils.saveItem,
      operation: operationTypes["DELETE"],
      tableName: process.env.CONTENT_JOURNAL_TABLE,
      util: HandlerUtils.touchJournal
    }
  };

  const collectionConfigs = {
    "POST": {
      schema: postCollection,
      paramsFunc: ContentUtils.generateJournalParams,
      databaseFunc: DatabaseUtils.saveItem,
      operation: operationTypes["CREATE"],
      tableName: process.env.CONTENT_JOURNAL_TABLE,
      util: HandlerUtils.touchJournal
    },
    "GET": {
      schema: getCollection,
      paramsFunc: ContentUtils.generateBlogSnapshotScanParams,
      databaseFunc: DatabaseUtils.scan,
      tableName: process.env.BLOG_SNAPSHOT_TABLE,
      util: HandlerUtils.touchSnapshot
    }
  };

  const config = EventUtils.chooseConfig(event, {collection: collectionConfigs, item: itemConfigs});

  if (event.httpMethod && event.httpMethod in config) {
    // Ex: HandlerUtils.touchJournal(validationUtils, event, {schema: "../../schema/collection/post.json",paramsFunc: contentUtils.generateJournalParams,databaseFunc: databaseUtils.saveItem,operation: operationTypes["CREATE"],tableName: process.env.CONTENT_JOURNAL_TABLE,util: HandlerUtils.touchJournal}, new AWS.DynamoDB.DocumentClient());
    config[event.httpMethod].util(validationUtils, event, config[event.httpMethod], new AWS.DynamoDB.DocumentClient(),
      callback, slugUtils, new Date().getTime());
  } else {
    const response = {
      statusCode: 405,
      body: JSON.stringify({
        message: `Invalid HTTP Method: ${event.httpMethod}`
      }),
    };
    callback(null, response);
  }
}
